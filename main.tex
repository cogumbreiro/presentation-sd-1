\documentclass[table]{beamer}
\setbeamertemplate{navigation symbols}{}
\setbeamertemplate{footline}[frame number]
%\usepackage[table]{xcolor}
\usepackage{listings}
\usepackage{tabu}
\usepackage{booktabs}
\usepackage{amssymb}
\usepackage{amsmath}
\usepackage{tikz}
\usepackage{xspace}
\usetikzlibrary{arrows}
\usetikzlibrary{shapes.callouts}
\input{macros-highlight-rect}
\input{macros-highlightlines}
\input{macros-draw-arrows}
\input{macros-lang}

\title
{Programming multicores safely: \\
deadlock-free barriers
}
\date{October 22, 2013}

\institute{Universidade de Lisboa}

\newcommand{\bigtitle}[1]{%
  \Huge%
  \begin{center}#1\end{center}%
}
\newenvironment{imgframe}[1]{
\usebackgroundtemplate{%
  \includegraphics[width=\paperwidth,height=\paperheight]{#1}}%
\begin{frame}[plain]%
}{%
\end{frame}%
\usebackgroundtemplate{}%
}

\newcommand{\eg}{\textit{e.g.},\@\xspace}
\newcommand{\ie}{\textit{i.e.,}\@\xspace}
\newcommand{\etal}{\textit{et al.}\@\xspace}
\newcommand{\cf}{\textit{cf.}\@\xspace}

\author{Tiago Cogumbreiro\\[1em]\footnotesize(advised by prof.\@ Francisco Martins)}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% listings configuration
\lstdefinelanguage{x10clocks}{
  morekeywords={async, newPhaser, drop, finish, await, arrive, let, in,
     join, unit, clock, advanceAll, skipAll, advance, for, int, val, new, unregister, nat, while
     atomic, do, un, ar},
  basicstyle=\sffamily,
  morecomment=[s]{\{-}{-\}},
  morecomment=[l]//,
  moredelim=[is][\color{red}]{*}{*},
  commentstyle=\color{black!60},
  flexiblecolumns=true,
  escapeinside={(*}{*)},
}

\lstset{language=x10clocks,captionpos=b}




%\setbeamercolor{normal text}{fg=white, bg=black}
%\setbeamercolor{section in toc}{fg=white}
%\setbeamercolor{section in toc}{bg=white}


% \AtBeginSection[] % Do nothing for \se
% {
%   \begin{frame}<beamer>
%     \frametitle{Outline}
%     \tableofcontents[currentsection]
%   \end{frame}
% }

% %\usefonttheme{structurebold}

\begin{document}

\begin{frame} % 10s
\titlepage
\end{frame}

%%%%%%%%%%%
% Why:
%%%%%%%%%%%


%  Technology, Market and Cost Trends 2012. Bernd Panzer-Steindel. CERN/IT. http://bit.ly/15WvVT2
\begin{frame} % 1m5s (1m25s)
  \frametitle{The multicore era: more cores}
  \includegraphics[width=\textwidth]{cores-per-die}

  {\footnotesize
    Source: \href{http://isscc.org/doc/2013/2013_Trends.pdf}{ISSCC '13}}
\end{frame}

\begin{frame} % 30s (2m40s)
  \frametitle{The multicore era: same speed}
  \includegraphics[width=\textwidth]{clock-frequency-2}

  {\footnotesize
  Source: \href{http://isscc.org/doc/2013/2013_Trends.pdf}{ISSCC '13}}
\end{frame}

\begin{frame} % 35s (3m10s)
  \bigtitle{
    {Languages \\
       incorporate parallel programming techniques\\
    \alert{(and concurrency failures)}}
  }
\end{frame}

\begin{frame}
  \frametitle{Research opportunity}
  \begin{itemize}
  \item scientific computing needs formal methods (Loh '10)
  \item new languages to tackle old problems: X10, HJ, OpenMP, UPC, Chapel, Fortress
  \item for example Java incorporated X10 constructs
  \end{itemize}
\end{frame}

%%%%%%%%%%%
% What?
%%%%%%%%%%%
\begin{frame} % 10s (20s)
  \bigtitle{Increase programmers' \emph{productivity}:\\
    by mitigating concurrency failures}
\end{frame}


\begin{frame}
  \frametitle{Bottom up approach: handle barrier synchronisation}
  \begin{itemize}
  \item a cornerstone of parallel programming
  \item SPLASH-2 parallel applications benchmark ---
    11 out of 12 use barriers (\href{http://dx.doi.org/10.1145/225830.223990}{Woo \etal '95})
  \item Java, C$\sharp$, POSIX-threads, $\dots$
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Contributions}
  A study on safety properties of \alert{barrier synchronisation}:
  \begin{enumerate}
  \item \textbf{a theoretical framework to study barriers}
  \item \textbf{established deadlock-freedom for a fragment of X10/HJ}
  \item a general deadlock detection algorithm for barriers (next talk!)
  \end{enumerate}
 \textbf{Outcome:} a \emph{comprehensive} solution to handle barrier deadlocks
\end{frame}

\begin{frame}
  \bigtitle{An introduction to barriers}
\end{frame}

\begin{frame}[fragile] % 30s (6m45s)
  \frametitle{An X10 program}

\begin{minipage}{0.4\linewidth}
\begin{lstlisting}
val c1 = Clock.make();

// (*\color{red}T1*):
async clocked(c1) {
  f1();
  c1.advance();
  f2();}
// (*\color{blue}T2*):
async clocked(c1) {
  g1();
  c1.advance();
  g2();}
\end{lstlisting}
\end{minipage}
%
\begin{minipage}{0.5\linewidth}
  \tikzstyle{exec}=[line width=2mm,round cap-round cap]
  \tikzstyle{barr}=[line width=1mm,draw opacity=0.6,olive]
  \begin{tikzpicture}
    \node[above] at (0,0) {\color{red}T1};
    \path (0,0) edge[exec,red] (0,-1);
    \node[left] at (0,-0.2) {f1()};
    \node[left] at (0,-0.7) {c1.advance()};
    \node[left] at (0,-2.4) {f2()};

    \path (0,-2) edge[exec,red] (0,-3);
    
    \node[above] at (2,0) {\color{blue}T2};
    \path (2,0) edge[exec, blue] (2,-3);
    \node[right] at (2,-1.2) {g1()};
    \node[right] at (2,-1.7) {c1.advance()};
    \node[right] at (2,-2.4) {g2()};
    
    \path (-2.3,-2) edge[barr] (4.3,-2);

    \node[above,color=gray!50] at (1,1) {Time};
    \path (1,1) edge[gray!50,->] (1,-3.5);
  \end{tikzpicture}
\end{minipage}
\end{frame}

\begin{frame}[fragile] % 30s (6m45s)
  \frametitle{An X10 program that deadlocks}
  \begin{minipage}{0.37\linewidth}
\begin{lstlisting}[linebackgroundcolor ={\btLstHL{6,11}{\color{yellow}}}]
val c1 = Clock.make();
val *c2* = Clock.make();
// T1:
async clocked(c1, *c2*) {
  f1();
  c1.advance();
  f2();}
// T2:
async clocked(c1, *c2*) {
  g1();
  *c2*.advance();
  g2();}
\end{lstlisting}
\end{minipage}
\hfill
\begin{minipage}{0.4\linewidth}
  \tikzstyle{dep}=[line width=0.5mm,->]
  \begin{tikzpicture}
    \node[circle,fill=red!30,minimum size=2em] (t1) at (0,0) {T1};
    \node[circle,fill=blue!30,minimum size=2em] (t2) at (3,0) {T2};
    \path (t1) edge[dep, bend left] node[above] {c1} (t2);
    \path (t2) edge[dep, bend left] node[below] {c2} (t1);
  \end{tikzpicture}
\end{minipage}
\end{frame}

\begin{frame}[fragile] % 30s (6m45s)
  \frametitle{An X10 program that does not deadlock}
\begin{lstlisting}
val c1 = Clock.make();
val c2 = Clock.make();
// T1:
async clocked(c1, c2) {
  f1();
  *Clock.advanceAll();*
  f2();}
// T2:
async clocked(c1, c2) {
  g1();
  *Clock.advanceAll();*
  g2();}
\end{lstlisting}
\end{frame}

\begin{frame}
  \frametitle{Split-phase synchronisation}
    \begin{itemize}
    \item advance: resume + await
    \item resume initiates synchronisation
    \item await waits for others
    \item overlap computation with synchronisation
    \item $\checkmark$ X10, $\checkmark$ Java , $\times$ C$\sharp$, $\times$ MPI
    \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Related work on barrier formalization}
  \begin{itemize}
  \item Saraswat \etal{} '05: X10 semantics, no safety properties verified
  \item Aditya \etal{} '95, Lee \etal{} '10: work on a limited form of
    barrier synchronisation (fork/join)
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{What we did for X10}
  \begin{description}
  \item[operational semantics] to define what instructions do 
  \item[type system] to specify valid behaviours
  \end{description}
\end{frame}

\begin{frame}
  \frametitle{Operational semantics: resume}
  \begin{tikzpicture}[overlay,remember picture]
    \uncover<1>{
      \node[fill=yellow] (note1) 
      at (5.5,1) {Before execution (expected state)};
      \draw (note1) edge[->] (5.5,0);
      \node[fill=green!30] (note2)
      at (5.5,-2) {After execution (resulting state)};
      \draw (note2) edge[->] (5.5,-1);
    }
    \node<2> (note3) 
    at (5.5,1) {The state of all barriers};
    \draw<2> (note3) edge[->] (4.2,0);

    \node<3> (note3) 
    at (5.5,1) {The state of the task};
    \draw<3> (note3) edge[->] (8,0);

    \node<4> (note3) 
    at (5.5,1) {The state of barrier c};
    \draw<4> (note3) edge[->] (4.6,0);
  \end{tikzpicture}
  \begin{equation*} 
    \begin{array}{lll}
      &
      \uncover<1>{\tikzmarkin{st1}}
      \big( 
      \uncover<2>{\tikzmarkin{c}}
      C\update c {
        \uncover<4>{\tikzmarkin{barr}}
        \heapClock n P R
        \uncover<4>{\tikzmarkend{barr}}
      }
      \uncover<2>{\tikzmarkend{c}},
      & 
      \uncover<3>{\tikzmarkin{st1}}
      {\activity {V} \Iresume}
      \uncover<1,3>{\tikzmarkend{st1}}
      \big)\\
      \rightarrow_l
      &
      \renewcommand{\bcol}{green!0}
      \renewcommand{\fcol}{green!30}
      \uncover<1>{\tikzmarkin{st2A}}
      \big( {C\update c {\heapClock {n} {P} {R\cup l}}},& {\activity {V} \unitV}
      \uncover<1>{\tikzmarkend{st2A}}
      \big)
    \end{array}
    \end{equation*}

    \vspace{3em}
    
    \uncover<5>{
    Task T1 and T2 initiate \lstinline|c.advance()|, where $P = \{T1,T2\}$:
    \begin{center}      
    \begin{tikzpicture}
      \node (s1) at (0,0) {$\heapClock 1 P \emptyset$};
      \node[right=of s1] (s2) {$\heapClock 1 P {\{T2\}}$};
      \node[right=of s2] (s3) {$\heapClock 1 P {\{T1,T2\}}$};
      \draw (s1) edge[->] node[above] {T2} (s2);
      \draw (s2) edge[->] node[above] {T1} (s3);
    \end{tikzpicture}
    \end{center}
    }
\end{frame}

\begin{frame}
  \frametitle{Operational semantics: wait (case 1)}
  \begin{equation*} 
    \begin{array}{lll}
      &\big( {C\update c {\heapClock n P P}},& {\activity {V\update c n} \Iwait} \big)\\
      \rightarrow_l
      &\big( {C\update c {\heapClock {n+1} P \emptyset}},& {\activity {V\update c {n+1}} \unitV} \big)
    \end{array}
    \end{equation*}  

    \vspace{2em}
    T2 executes wait: 
    \begin{center}      
    \begin{tikzpicture}
      \node (s1) at (0,0) {$\heapClock {1} {\{T1,T2\}} {\{T1,T2\}}$};
      \node[right=of s1] (s2) {$\heapClock 2 {\{T1,T2\}} {\emptyset}$};
      \draw (s1) edge[->] node[above] {T2} (s2);
    \end{tikzpicture}

    \begin{tikzpicture}
      \node (s1) at (0,0) {$\activity {\update c 1} \Iwait$};
      \node[right=of s1] (s2) {$\activity {\update c 2} \unitV$};
      \draw (s1) edge[->] node[above] {T2} (s2);
    \end{tikzpicture}
    \end{center}
\end{frame}

\begin{frame}
  \frametitle{Operational semantics: wait (case 2)}
  \begin{equation*} 
    \frac{
      C(c) = \heapClock {n + 1} P R
    }{
      \big( C,{\activity {V\update c n} \Iwait} \big)
      \rightarrow_l
      \big( C,{\activity {V\update c {n+1}} \unitV} \big)
      }
    \end{equation*}  
    
    \vspace{2em}

    T1 executes wait and $C(c) = \heapClock 2
    {\{T1,T2\}} {\emptyset}$:
    \begin{center}    
    \begin{tikzpicture}
      \node (s1) at (0,0) {$\activity {\update c 1} \Iwait$};
      \node[right=of s1] (s2) {$\activity {\update c 2} \unitV$};
      \draw (s1) edge[->] node[above] {T1} (s2);
    \end{tikzpicture}
    \end{center}
\end{frame}

\begin{frame}
  \frametitle{Type system}
  \begin{tikzpicture}[overlay,remember picture]
    \uncover<1>{
      \node[fill=yellow] (note1) 
      at (5.5,1) {if this holds};
      \draw (note1) edge[->] (5.5,0);
      \node[fill=green!30] (note2)
      at (5.5,-2) {then this holds};
      \draw (note2) edge[->] (5.5,-1);
    }
    \uncover<2>{
      \node[fill=yellow] (note1) 
      at (5.5,1) {input};
      \draw (note1) edge[->] (4,0);
      \node[fill=green!30] (note2)
      at (5.5,-2) {output};
      \draw (note2) edge[->] (6.5,-1);
    }
  \end{tikzpicture}
  Resume:
  \begin{equation*}
    \frac{
      \uncover<1>{\tikzmarkin{v}}      
      %
      \uncover<2>{\tikzmarkin{know}}
      \Gamma; \mathcal P
      \uncover<2>{\tikzmarkend{know}}
      %
      \vdash v \colon
      %
      \alpha
      \qquad
      \uncover<3>{\tikzmarkin{inA}}
      \alpha \notin \mathcal R
      \uncover<3>{\tikzmarkend{inA}}
      \uncover<1>{\tikzmarkend{v}}
    }{
      \uncover<1>{%
        \renewcommand{\bcol}{green!0}%
        \renewcommand{\fcol}{green!30}%
        \tikzmarkin{st2}}
      \Gamma; \mathcal P; \mathcal R
      \vdash \Iresume[v] \colon
      %
      \uncover<2>{\tikzmarkin{res}}
      (\Tunit, \mathcal P,
        \renewcommand{\bcol}{yellow!0}
        \renewcommand{\fcol}{yellow}
      \uncover<3>{
        \tikzmarkin{addA}
      }
      \mathcal R \cup \alpha
      \uncover<3>{\tikzmarkend{addA}}
      )
      \uncover<2>{\tikzmarkend{res}}
      \uncover<1>{\tikzmarkend{st2}}
    }
  \end{equation*}
  \uncover<4>{
  Await:
  \begin{equation*}
    \Gamma;
    \tikzmarkin{eq}
    \mathcal P; \mathcal P
    \tikzmarkend{eq}
    \vdash \nextk \colon (\Tunit,
    \tikzmarkin{empty}
    \mathcal P;
    \emptyset
    \tikzmarkend{empty}
    )
  \end{equation*}
  }
\end{frame}

\begin{frame}
  \frametitle{Formalizing X10}

  Francisco Martins, Vasco T. Vasconcelos, and Tiago
  Cogumbreiro. \textbf{Types for X10 Clocks}. [PLACES'10]
  
  \begin{itemize}
  \item (alternative) specification of the programming model \& clocks
  \item theoretical framework to reason about barrier programs
%  \item 1 year
  \end{itemize}
\end{frame}

% 3m 10s

%%%%%%%%%%%
% How?
%%%%%%%%%%%

\begin{frame}
  \bigtitle{Research opportunity:\\
    deadlock-freedom is claimed,\\ but not proved}
\end{frame}

\begin{frame}
  \frametitle{Related work on barrier deadlock-freedom}
  \begin{itemize}
  \item Lee \etal{} '10: limited form of barrier synchronisation (fork/join)
  \item Le \etal{} '13: some safety properties, no deadlock-freedom
  \end{itemize}
\end{frame}

% \begin{frame}
%   \frametitle{The deadlock problem: some statistics}
%   \begin{itemize}
%   \item 3\% of Sun's bug database involve the keyword deadlock (Naik \etal{} '09)
%   \item out of 105 concurrency bugs, \alert{30\%} were deadlocks (Lu \etal{} '08)
% %  \item out of 80 concurrency bugs, \alert{40\%} were deadlocks (Fonseca \etal{} '10)
%   \end{itemize}
% \end{frame}

\begin{frame}
  \frametitle{Phasers: extended clocks}
  \begin{itemize}
  \item some extensions w.r.t.\@ clocks (next talk!)
  \item there is also a claim of deadlock-freedom
  \end{itemize}
\end{frame}


\begin{imgframe}{habanero}
\end{imgframe}

\begin{frame}
  \frametitle{Visiting Rice University}
  \begin{itemize}
  \item Vivek Sarkar (and some IBM employees)
  \item Habanero team (20 people)
  \item 2 semesters of seminars and Habanero meetings
  \item 2 talks
  \item school of thought:  pragmatism
  \item privileged understanding of phasers
  \end{itemize}
\end{frame}


\begin{frame}
  \frametitle{Deadlock-freedom invariant}
  $$
  \forall c1,c2\colon V_{T1}(c1) - V_{T2}(c1) = V_{T1}(c2) - V_{T2}(c2)
  $$
  \begin{enumerate}
  \item barriers are shared from parent-task to child-task
  \item all barriers are advanced at once
  \item there is a phase difference
  \item use this phase difference to establish an ordering between tasks
  \item the least task advances
  \end{enumerate}
\end{frame}

\begin{frame}
  \frametitle{Results}
  \begin{itemize}
  \item subject reduction: reduction preserves validity
  \item progress: all valid states execute 
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Conclusions}

  Tiago Cogumbreiro, Francisco Martins, and Vasco
  T. Vasconcelos. \textbf{Coordinating phased activities while
    maintaining progress}. [COORDINATION'13]

  \begin{itemize}
  \item a single theoretical framework for multiple barrier constructs
%  \item study asymmetric barrier synchronisation
%  \item a study on bounded and unbounded signal/wait all
  \item established progress and type preserving (deadlock-freedom)
%  \item 1.5 years
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Future work}
  \begin{itemize}
  \item collaboration with Rice University (ongoing)
  \item collaboration with Imperial College of London
  \item deadlock detection (submitted)
  \item incorporate imperative constructs and submit it to a journal (1 month of work)
  \item thesis (2 month of work)
  \end{itemize}
\end{frame}

\begin{frame}
\titlepage
\end{frame}

\begin{frame}
  \frametitle{References}
  \begin{itemize}
  \item S. Aditya, J.E. Stoy, and
    Arvind. \href{http://dx.doi.org/10.1145/224164.224206}{\textit{Semantics
        of barriers in a non-strict, implicitly-parallel
        language}}. FPCA '95.
  % \item P. Fonseca, C. Li, V. Singhal, R. Rodrigues. \textit{A Study of the Internal and External Effects of Concurrency Bugs.} DSN '10.
  \item D.-K. Le, W.-N. Chin, Y.-M. Teo. \textit{Verification of
      static and dynamic barrier synchronization using bounded
      permissions}. ICFEM '13.
  \item J.K. Lee and J. Palsberg.
    \href{http://dx.doi.org/10.1145/1693453.1693459}{\textit{Featherweight
        X10: a core calculus for async-finish parallelism}}. PPoPP '10.
  \item
    E. Loh. \href{http://dx.doi.org/10.1145/1810226.1820518}{\textit{The
        Ideal HPC Programming Language}}. ACM Queue. 2010.
  % \item S. Lu, S. Park, E. Seo,
  %   Y. Zhou. \href{http://dx.doi.org/10.1145/1346281.1346323}{\textit{A
  %       comprehensive study on real world concurrency bug
  %       characteristics}}. ASPLOS '08.
  % \item M. Naik, C.-S. Park, K. Sen, D. Gay. \textit{Effective Static
  %     Deadlock Detection}. ICSE '09.
  \item V. Saraswa and
    R. Jagadeesan. \href{http://dx.doi.org/10.1007/11539452_28}{\textit{Concurrent
        clustered programming}}. CONCUR '05.
  \end{itemize}
\end{frame}

\end{document}
